import React from 'react';

var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var MainFrame = require("./components/MainFrame");
var Users = require("./components/Users");
var Login = require("./components/Login");
var DisabledUsers = require("./components/DisabledUsers");
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import {hashHistory, Redirect, IndexRoute} from 'react-router'

ReactDOM.render(
    <LocaleProvider locale={enUS}>
        <Router history={hashHistory}>
            <Redirect from="/" to="/login"/>
            <Route path="/login" component={Login}/>
            <Route path="/insports" component={MainFrame}>
                <Route path="/users" component={Users}/>
                <Route path="/disabledUsers" component={DisabledUsers}/>
            </Route>
        </Router>
    </LocaleProvider>

    , document.getElementById('renderData')
);
