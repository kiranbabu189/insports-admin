function getServerLocation(){
	return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '') + '/';
}
var imageUrl = 'https://52f06137f08f0a562a4afdbf23f213ea.s3-ap-south-1.amazonaws.com/'
module.exports = {
    // server: getServerLocation(),
    // server: 'http://192.168.0.22:8090/admin-api/',
    server: 'http://18.216.190.75/admin-api/',
    id:null,
    getS3Url: (image, type) => {
        switch (type) {
            case 'avatar': return imageUrl + image + '152X152.jpg';
            case 'thumb': return imageUrl + image + '/48X48.jpg'
            case 'loader': return imageUrl + image + '5X5.jpg'
            case 'dropDown': return imageUrl + image + '/35X35.jpg'
            case 'follow': return imageUrl + image + '/15X15.jpg'
            case 'card': return imageUrl + image + '/70X70.jpg'
            case 'post': return imageUrl + image + '692X398.jpg'
            case 'cover': return imageUrl + image + '739X425.jpg'
            case 'gallery': return imageUrl + image + '160X160.jpg'
            case 'eventPost': return imageUrl + image + '692X398.jpg'
            case 'eventSmall': return imageUrl + image + '341X125.jpg'
        }
    }
}