import React, {Component} from 'react';
import {hashHistory, Link} from 'react-router'
import _ from "lodash"
import "antd/dist/antd.min.css";
import "public/css/style.css";
import "public/css/custom.css";
import 'react-select/dist/react-select.css';

export default class MainFrame extends Component {

    render() {
        const {location} = this.props
        return (
            <div className="container mainframe-wrapper">
                <div className="panel panel-default">
                    <ul className="nav nav-tabs">
                        <li className={`nav ${location.pathname == '/users' ? 'active' : ''}`}>
                            <Link
                                to='/users'
                                className="nav-links"
                            >
                                Users
                            </Link>
                        </li>
                        <li className={`nav ${location.pathname == '/disabledUsers' ? 'active' : ''}`}>
                            <Link
                                to='/disabledUsers'
                                className="nav-links"
                            >
                                Disabled users
                            </Link>
                        </li>
                    </ul>
                    {this.props.children}
                </div>
            </div>
        );
    }
};
