import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/MainFrame/constants/TabStateConstant';
import config from 'utils/config';


var TabStateAction = function(){
}

TabStateAction.prototype = {
    isClicked: function(resp){
		    AppDispatcher.dispatch({
		       actionType: Constants.TABSTATE_RESPONSE_RECIEVED,
		       data: resp
		    })
    }
       
}


module.exports = new TabStateAction();




