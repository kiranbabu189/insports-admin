var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
import Constants from 'components/MainFrame/constants/PreloadConstants';
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';

function parseResponse(resp){
    console.log("resp...........",resp)
}

var onPreloadStore = assign({},EventEmitter.prototype,{
   emitChangeEvent: function(event) {
       this.emit(event);
   },
   bind: function(callback) {
       this.on(RESPONSE_CHANGE_EVENT, callback);
   },
   unbind: function(callback) {
       this.removeListener(RESPONSE_CHANGE_EVENT, callback);
   },
   getPreloadData:function(){
        return majorTab;
   }
});

Dispatcher.register(function(action){

   switch (action.actionType) {
       case Constants.PRELOAD_RESPONSE_RECIEVED:
           var resp = action.data;
           parseResponse(resp)
           console.log("onPreloadStore....",resp)
           onPreloadStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
       default:
   }


});

module.exports = onPreloadStore;