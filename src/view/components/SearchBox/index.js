import React, {Component} from 'react';
import Select from 'react-select';

export default class SearchBox extends Component{
    render(){
        return(
            <Select
                openOnFocus={true}
                name="form-field-name"
                value="one"
                options={this.props.suggestions}
                onChange={this.props.onChange}
                className="setting__sport"
                onInputChange={this.props.onInputChange}
                optionRenderer={this.props.customOption}
            />
        )
    }
}