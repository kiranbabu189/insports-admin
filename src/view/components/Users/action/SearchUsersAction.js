import AppDispatcher from 'dispatcher/dispatcher';
import Constant from '../constant/Constant';
import config from 'utils/config'

export default class SearchUsersAction {
    static search(query) {
        $.ajax({
            url: config.server + 'searchUser?q=' + query, //config.server + 'search',
            // url: 'http://localhost:3000/data',
            type: 'GET',
            dataType: 'JSON',
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
                AppDispatcher.dispatch({
                    actionType: Constant.SEARCH_USERS,
                    data: resp
                });
            },
            error: function (err) {
                console.log("Search Results: Ajax error ", err);
            }
        });
    }
}


