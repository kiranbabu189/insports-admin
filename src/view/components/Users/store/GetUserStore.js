var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;

import Constant from '../constant/Constant';

var response = {};

var assign = require('object-assign');

function parseResponse(resp){
    response = resp;
}

var GetUserStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(Constant.USERS_LIST, callback);
    },
    unbind: function(callback) {
        this.removeListener(Constant.USERS_LIST, callback);
    },
    getResponse:function(){
        return response;
    }
});

Dispatcher.register(function(action){

    switch (action.actionType) {
        case Constant.USERS_LIST:
            var resp = action.data;
            parseResponse(resp)
            GetUserStore.emitChangeEvent(Constant.USERS_LIST)
        default:
    }


});

module.exports = GetUserStore;
