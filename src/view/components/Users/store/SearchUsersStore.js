var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;

import Constant from '../constant/Constant';

var response = {};

var assign = require('object-assign');

function parseResponse(resp) {
    response = resp;
}

var SearchUsersStore = assign({}, EventEmitter.prototype, {
    emitChangeEvent: function (event) {
        this.emit(event);
    },
    bind: function (callback) {
        this.on(Constant.SEARCH_USERS, callback);
    },
    unbind: function (callback) {
        this.removeListener(Constant.SEARCH_USERS, callback);
    },
    getResponse: function () {
        return response;
    }
});

Dispatcher.register(function (action) {

    switch (action.actionType) {
        case Constant.SEARCH_USERS:
            var resp = action.data;
            parseResponse(resp)
            SearchUsersStore.emitChangeEvent(Constant.SEARCH_USERS)
        default:
    }


});

module.exports = SearchUsersStore;
