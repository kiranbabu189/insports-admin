import React, { Component } from 'react';
import GetUserAction from './action/GetUserAction'
import DisableUsersAction from './action/DisableUsersAction'
import SearchUsersAction from './action/SearchUsersAction'
import GetUserStore from './store/GetUserStore'
import DisableUsersStore from './store/DisableUsersStore'
import SearchUsersStore from './store/SearchUsersStore'
import ModalClass from '../ModalClass'
import SearchBox from '../SearchBox'
import SearchSuggest from '../SearchSuggest'
import Filters from '../Filters'
import { DatePicker, Select, Collapse, Table, Radio, Button, Spin } from 'antd';
import moment from 'moment';
import config from '../../utils/config'
const Option = Select.Option;
const Panel = Collapse.Panel;
const RadioGroup = Radio.Group;

export default class Users extends Component {
    constructor() {
        super();
        this.state = {
            users: [],
            modalVisible: false,
            bulkModalVisible: false,
            record: 0,
            date: moment(new Date(), 'DD-MM-YYYY'),
            type: '',
            emailVerify: '',
            gender: '',
            filters: {
                date: '',
                type: '',
                emailVerify: '',
                gender: ''
            },
            size: 10,
            from: 0,
            checkBoxUsers: [],
            disableUsersLoading: false,
            selectedRowKeys:[],
            suggestions:[]
        }
        GetUserAction.getUsers(this.state.from, this.state.size);
        this.handleResponse = this.handleResponse.bind(this);
        this.handleFilters = this.handleFilters.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
        this.bulkHandleModal = this.bulkHandleModal.bind(this);
        this.disableUser = this.disableUser.bind(this);
        this.handleDisableResponse = this.handleDisableResponse.bind(this);
        this.getUserList = this.getUserList.bind(this);
        this.callPaginate = this.callPaginate.bind(this);
        this.handleSearchResponse = this.handleSearchResponse.bind(this);
    }

    componentWillMount() {
        GetUserStore.bind(this.handleResponse)
        DisableUsersStore.bind(this.handleDisableResponse);
        SearchUsersStore.bind(this.handleSearchResponse);
    }

    componentWillUnmount() {
        GetUserStore.unbind(this.handleResponse)
        DisableUsersStore.unbind(this.handleDisableResponse);
        SearchUsersStore.unbind(this.handleSearchResponse)
    }
    handleDisableResponse(){
        this.setState({
            disableUsersLoading:false,
            bulkModalVisible:false,
            checkBoxUsers:[]
        })
        GetUserAction.getUsers(this.state.from, this.state.size);
    }
    handleSearchResponse(){
        let response = SearchUsersStore.getResponse();
        // console.log("suggestions", suggestions);
        if (response['status'] == 'ok'){
            let suggestionsJsx = [];
            _.map(response['content']['users'], (list, key) => {
                if (list.type != 'org') {
                    suggestionsJsx.push({
                        value: list.name,
                        label: list.name,
                        email: list.emailId,
                        img: list.thumb,
                        id: list.id,
                        type: list.type
                    })
                }
            });
            this.setState({
                suggestions: suggestionsJsx
            });
        }
    }
    customOption(e) {
        // debugger;
        if (e.value != '') {
            return (
                <SearchSuggest
                    img={e.id == undefined ? "img/avatar.png" : config.getS3Url(e.id, 'dropDown')}
                    value={e.value}
                />)
        }
    }
    handleResponse() {
        let response = GetUserStore.getResponse();
        if (response['status'] == 'ok') {
            this.setState({
                users: response['content']['users'],
                total: response['content']['total'],
                selectedRowKeys:[]
            })
        }
    }

    handleModal() {

    }

    handleFilters(type, value) {
        let filters = this.state.filters;
        filters[type] = value;

        this.setState({
            filters,
            [type]: value
        })
    }

    removeFilter(key) {
        let filters = this.state.filters;
        filters[key] = '';
        this.setState({
            filters,
            [key]: key == 'date' ? moment(new Date(), 'DD-MM-YYYY') : ''
        });
        // debugger;
    }

    bulkHandleModal() {
        this.setState({
            bulkModalVisible: true
        })
    }

    callPaginate(e) {
        if (e.current * 10 != this.state.total){
            GetUserAction.getUsers(e.current * 10, this.state.size);
            this.setState({
                from: e.current
            })
        }
    }
    getUserList(e){
        console.log('search',e)
        SearchUsersAction.search(e)
    }
    disableUser() {
        var toSendObj = {
            userList: []
        }
        _.map(this.state.checkBoxUsers, (value, key) => {
            toSendObj['userList'].push(this.state.users[value['key']]['docId'])
        })
        var list = toSendObj;
        DisableUsersAction.disableUsers(list);
        this.setState({
            disableUsersLoading: true
        })

        // debugger;
    }
    
    render() {
        const columns = [{
            title: 'User-Id',
            dataIndex: 'id',
            key: 'id'

        }, {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        }, {
            title: 'Type',
            dataIndex: 'type',
            key: 'type',
        }, {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <span>
                    <a onClick={() => this.setState({ bulkModalVisible: true, checkBoxUsers: _.castArray(record) })}>Disable</a>
                    <span className="ant-divider" />
                </span>
            ),
        }];
        var tableData = []
        _.map(this.state.users, (value, key) => {
            tableData.push({
                key: key,
                id: value.id,
                email: value.email,
                type: value.type == 'user' ? 'Individual' : 'Organization'
            })
        });
        const selectedRowKeys = this.state.selectedRowKeys
        const rowSelection = {
            selectedRowKeys,
            onChange: (selectedRowKeys, selectedRows) => {
                this.setState({ selectedRowKeys,checkBoxUsers: selectedRows })
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User', // Column configuration not to be checked
            }),
            onSelectAll: (e) => console.log('sdsd', e)
        };
        console.log("search suggestion", this.state.suggestions)
        return (
            <div style={{ paddingTop: '10px' }} className="content-wrapper">
                {tableData.length > 0 && <div>
                    <Collapse onChange={(e) => {
                        console.log(e)
                    }}>
                        <Panel header={"Filters"} key="1">
                            <div className="row filters">
                                <div className="col-md-8">
                                    <Filters filters={this.state.filters} removeFilter={this.removeFilter} />
                                </div>
                                <div className="col-md-4">
                                    <a className="text-right" onClick={() => this.bulkHandleModal()}>
                                        Perform bulk action
                                    </a>
                                </div>
                            </div>
                            <div className="row filters">
                                <div className="col-md-3">
                                    <label>Join date : </label><DatePicker
                                        value={moment(this.state.date)}
                                        size="small"
                                        disabledDate={(current) => {
                                            return current && current.valueOf() >= Date.now()
                                        }}
                                        onChange={(date, dateString) => this.handleFilters('date', dateString)} />
                                </div>
                                <div className="col-md-3">
                                    <label>User type : </label>
                                    <Select defaultValue="" value={this.state.type} style={{ width: 120 }}
                                        onChange={(e) => this.handleFilters('type', e)}>
                                        <Option value="Individual">Individual</Option>
                                        <Option value="Organization">Organization</Option>
                                    </Select>
                                </div>
                                <div className="col-md-3">
                                    <label>Email verified : </label>
                                    <Select defaultValue="" value={this.state.emailVerify} style={{ width: 120 }}
                                        onChange={(e) => this.handleFilters('emailVerify', e)}>
                                        <Option value="yes">Yes</Option>
                                        <Option value="no">No</Option>
                                    </Select>
                                </div>
                                <div className="col-md-3">
                                    <label>Gender : </label>
                                    <Select defaultValue="" value={this.state.gender} style={{ width: 120 }}
                                        onChange={(e) => this.handleFilters('gender', e)}>
                                        <Option value="Male">Male</Option>
                                        <Option value="Female">Female</Option>
                                    </Select>
                                </div>
                            </div>
                        </Panel>
                    </Collapse>
                    <div className="disable-button">
                        {this.state.checkBoxUsers.length > 0 && <div><Button onClick={() => this.setState({ bulkModalVisible: true })} type="primary" size="small" >
                            Disable selected users
                        </Button> {/*<Button type="primary" size="small" >
                            Refresh list
                        </Button>*/}</div>
                        }
                    </div>
                    <SearchBox
                        onChange={this.getUserList}
                        onInputChange={this.getUserList}
                        customOption={this.customOption}
                        suggestions={this.state.suggestions}
                        suggestionSelectHandler={this.suggestionSelectHandler}
                        customOption={this.customOption}
                        needButton={false}
                    />
                    <Table
                        rowSelection={rowSelection}
                        columns={columns}
                        dataSource={tableData}
                        pagination={{ total: Number(this.state.total) }}
                        onChange={(e) => {
                            this.callPaginate(e)
                        }}
                    />
                    <ModalClass
                        header={"Perform action"}
                        modalVisible={this.state.modalVisible}
                        handleModal={() => this.setState({ modalVisible: false })}
                    >
                        <p>
                            <label>User id : </label>
                            {tableData && tableData[this.state.viewId] && tableData[this.state.viewId]['id']}
                            &nbsp;&nbsp;&nbsp;<label>Email : </label>
                            {tableData && tableData[this.state.viewId] && tableData[this.state.viewId]['email']}
                        </p>
                        <p>
                            <RadioGroup onChange={this.onChange} value={this.state.value}>
                                <Radio value={1}>Disable account</Radio>
                                <Radio value={2}>Enable account</Radio>
                            </RadioGroup>
                        </p>
                    </ModalClass>
                    <ModalClass
                        header={"Perform operation"}
                        modalVisible={this.state.bulkModalVisible}
                        handleModal={() => { this.disableUser(); }}
                        handleModalClose={() => this.setState({ checkBoxUsers: [], bulkModalVisible: false })}
                    >
                        <div className="container-fluid">
                            <Spin spinning={this.state.disableUsersLoading} tip="Disabling users">
                                <h5>Total users for operation : {this.state.checkBoxUsers.length}</h5>
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>User-id</th>
                                            <th>Email</th>
                                            <th>Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {_.map(this.state.checkBoxUsers, (value, key) => {
                                            return <tr key={key}><td>{value.id}</td><td>{value.type}</td><td>{value.email}</td></tr>
                                        })}
                                    </tbody>
                                </table>
                            </Spin>
                        </div>
                    </ModalClass>

                </div>
                }
            </div>
        )
    }
}