var keyMirror = require('keymirror');

module.exports = keyMirror({
    USERS_LIST: null,
    DISABLE_USERS:null,
    SEARCH_USERS:null
});
