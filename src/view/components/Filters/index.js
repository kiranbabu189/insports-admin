import React, {Component} from 'react';
import {Tag} from 'antd';

export default class Filters extends Component {
    render() {
        // console.log('filters',this.props.filters)
        return (
            <div className="filter-headers">
                <label>Applied filters</label>
                <div className="filter-list">{_.map(this.props.filters, (value, key) => {
                    return <Tag color="green" closable onClose={(e) => {
                        e.preventDefault();
                        this.props.removeFilter(key)
                    }}>{`${key} : ${value}`}</Tag>
                })}
                </div>
            </div>
        )
    }
}