import React from 'react';
import ImageLoader from "react-imageloader";
import { Icon } from 'antd';

export default class SearchSuggest extends React.Component {
    render() {
        return (
            <div className="search-img-results">
                <div className="search-result-image">
                    <ImageLoader
                        className="search-img-loader"
                        src={this.props.img}
                        preloader={() => {
                            return <Icon type="loading" style={{ paddingTop: '3px', paddingLeft: '4px', fontSize: 20, color: 'green' }} />;
                        }}
                    >
                        <img src="img/avatar.png" alt="" />
                    </ImageLoader>
                </div>
                <div className="search-result-name">{this.props.value}</div>
            </div>
        )
    }
}