var keyMirror = require('keymirror');

module.exports = keyMirror({
    USERS_LIST: null,
    DISABLED_USERS: null,
    ENABLE_USERS:null
});
