import AppDispatcher from 'dispatcher/dispatcher';
import Constant from '../constant/Constant';
import config from 'utils/config'

export default class EnbleUsersAction {
    static enable(list) {
        $.ajax({
            url: config.server + 'enableUser',
            type: 'POST',
            dataType: 'JSON',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(list),
            success: function (resp) {
                AppDispatcher.dispatch({
                    actionType: Constant.ENABLE_USERS,
                    data: resp
                });
            },
            error: function (err) {
                console.log("Search Results: Ajax error ", err, JSON.stringify(userDoc));
                if (err.status === 401) {
                    alert(err.responseText)
                    hashHistory.push('/login');
                }
                AppDispatcher.dispatch({
                    actionType: Constant.ENABLE_USERS,
                    data: err
                });
            }
        });

    }
}


