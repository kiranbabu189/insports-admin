import React, { Component } from 'react';
import { DatePicker, Select, Collapse, Table, Radio, Button, Spin } from 'antd';
import GetDisabledUserAction from './action/GetDisabledUserAction';
import EnbleUsersAction from './action/EnbleUsersAction';
import GetDisabledUserStore from './store/GetDisabledUserStore';
import EnableUsersStore from './store/EnableUsersStore';
import ModalClass from '../ModalClass';

export default class DisabledUsers extends Component {
    constructor() {
        super();
        this.state = {
            users: [],
            total: 0,
            from: 0,
            size: 10,
            bulkModalVisible: false,
            disableUsersLoading: false,
            checkBoxUsers: [],
            selectedRowKeys: []
        }
        this.handleResponse = this.handleResponse.bind(this);
        this.callPaginate = this.callPaginate.bind(this);
        this.handleEnableResponse = this.handleEnableResponse.bind(this);
    }
    componentWillMount() {
        GetDisabledUserAction.getUsers(this.state.from, this.state.size);
        GetDisabledUserStore.bind(this.handleResponse);
        EnableUsersStore.bind(this.handleEnableResponse);
    }
    componentWillUnmount() {
        GetDisabledUserStore.unbind(this.handleResponse);
        EnableUsersStore.unbind(this.handleEnableResponse);
    }
    handleEnableResponse() {
        this.setState({
            disableUsersLoading: false,
            bulkModalVisible: false,
            checkBoxUsers: []
        })
        GetDisabledUserAction.getUsers(this.state.from, this.state.size);
    }
    handleResponse() {
        let response = GetDisabledUserStore.getResponse();
        if (response['status'] == 'ok') {
            this.setState({
                users: response['content']['users'],
                total: response['content']['total'],
                selectedRowKeys: []
            })
        }
    }
    callPaginate(e) {
        if (e.current * 10 != this.state.total) {
            GetDisabledUserAction.getUsers(e.current * 10, this.state.size);
            this.setState({
                from: e.current
            })
        }
    }
    disableUser() {
        var toSendObj = {
            userList: []
        }
        _.map(this.state.checkBoxUsers, (value, key) => {
            toSendObj['userList'].push(this.state.users[value['key']]['docId'])
        })
        var list = toSendObj;
        EnbleUsersAction.enable(list);
        this.setState({
            disableUsersLoading: true
        })

        // debugger;
    }
    render() {
        const columns = [{
            title: 'User-Id',
            dataIndex: 'id',
            key: 'id'

        }, {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        }, {
            title: 'Type',
            dataIndex: 'type',
            key: 'type',
        }, {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <span>
                    <a onClick={() => this.setState({ bulkModalVisible: true, checkBoxUsers: _.castArray(record) })}>Enable</a>
                    <span className="ant-divider" />
                </span>
            ),
        }];
        var tableData = []
        _.map(this.state.users, (value, key) => {
            tableData.push({
                key: key,
                id: value.id,
                email: value.email,
                type: value.type == 'user' ? 'Individual' : 'Organization'
            })
        });
        // debugger;
        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                this.setState({ checkBoxUsers: selectedRows })
            },
            getCheckboxProps: record => ({
                disabled: record.name === 'Disabled User', // Column configuration not to be checked
            }),
            onSelectAll: (e) => console.log('sdsd', e)
        };
        return (
            <div className="content-wrapper">
                <div className="disable-button">
                    {this.state.checkBoxUsers.length > 0 && <div><Button onClick={() => this.setState({ bulkModalVisible: true })} type="primary" size="small" >
                        Enable selected users
                        </Button> {/*<Button type="primary" size="small" >
                            Refresh list
                        </Button>*/}</div>
                    }
                </div>
                <Table
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={tableData}
                    pagination={{ total: Number(this.state.total) }}
                    onChange={(e) => {
                        this.callPaginate(e)
                    }}
                />
                <ModalClass
                    header={"Perform operation"}
                    modalVisible={this.state.bulkModalVisible}
                    handleModal={() => { this.disableUser(); }}
                    handleModalClose={() => this.setState({ checkBoxUsers: [], bulkModalVisible: false })}
                >
                    <div className="container-fluid">
                        <Spin spinning={this.state.disableUsersLoading} tip="Disabling users">
                            <h5>Total users for operation : {this.state.checkBoxUsers.length}</h5>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>User-id</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {_.map(this.state.checkBoxUsers, (value, key) => {
                                        return <tr key={key}><td>{value.id}</td><td>{value.type}</td><td>{value.email}</td></tr>
                                    })}
                                </tbody>
                            </table>
                        </Spin>
                    </div>
                </ModalClass>

            </div>
        )
    }
}