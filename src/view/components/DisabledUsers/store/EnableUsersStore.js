var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;

import Constant from '../constant/Constant';

var response = {};

var assign = require('object-assign');

function parseResponse(resp) {
    response = resp;
}

var EnableUsersStore = assign({}, EventEmitter.prototype, {
    emitChangeEvent: function (event) {
        this.emit(event);
    },
    bind: function (callback) {
        this.on(Constant.ENABLE_USERS, callback);
    },
    unbind: function (callback) {
        this.removeListener(Constant.ENABLE_USERS, callback);
    },
    getResponse: function () {
        return response;
    }
});

Dispatcher.register(function (action) {

    switch (action.actionType) {
        case Constant.ENABLE_USERS:
            var resp = action.data;
            parseResponse(resp)
            EnableUsersStore.emitChangeEvent(Constant.ENABLE_USERS)
        default:
    }


});

module.exports = EnableUsersStore;
