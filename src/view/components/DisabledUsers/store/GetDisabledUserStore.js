var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;

import Constant from '../constant/Constant';

var response = {};

var assign = require('object-assign');

function parseResponse(resp) {
    response = resp;
}

var GetDisabledUserStore = assign({}, EventEmitter.prototype, {
    emitChangeEvent: function (event) {
        this.emit(event);
    },
    bind: function (callback) {
        this.on(Constant.DISABLED_USERS, callback);
    },
    unbind: function (callback) {
        this.removeListener(Constant.DISABLED_USERS, callback);
    },
    getResponse: function () {
        return response;
    }
});

Dispatcher.register(function (action) {

    switch (action.actionType) {
        case Constant.DISABLED_USERS:
            var resp = action.data;
            parseResponse(resp)
            GetDisabledUserStore.emitChangeEvent(Constant.DISABLED_USERS)
        default:
    }


});

module.exports = GetDisabledUserStore;
