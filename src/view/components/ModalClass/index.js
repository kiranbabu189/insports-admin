import React, {Component} from 'react';
import {Modal} from 'antd';


export default class ModalClass extends Component{
    render(){
        let {modalVisible} = this.props
        return (
            <Modal
                title={this.props.header}
                visible={modalVisible}
                onOk={this.props.handleModal}
                onCancel={this.props.handleModalClose}
            >
                {this.props.children}
            </Modal>
        )
    }
}