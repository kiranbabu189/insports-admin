var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;

import Constant from '../constant/Constant';

var response = {};

var assign = require('object-assign');

function parseResponse(resp){
    response = resp;
}

var LoginStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(Constant.LOGIN, callback);
    },
    unbind: function(callback) {
        this.removeListener(Constant.LOGIN, callback);
    },
    getResponse:function(){
        return response;
    }
});

Dispatcher.register(function(action){

    switch (action.actionType) {
        case Constant.LOGIN:
            var resp = action.data;
            parseResponse(resp)
            LoginStore.emitChangeEvent(Constant.LOGIN)
        default:
    }


});

module.exports = LoginStore;
