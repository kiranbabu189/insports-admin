import React , {Component} from 'react';
import 'public/css/login.css';
import LoginAction from './action/LoginAction';
import LoginStore from './store/LoginStore';
import {hashHistory, Link} from 'react-router';
import { Button, notification } from 'antd';


export default class Login extends Component{
    constructor(){
        super()
        this.state = {
            id:'',
            password:'',
            buttonText:'Login'
        }
        this.loginCheck = this.loginCheck.bind(this);
        this.openNotification = () => {
            const args = {
                message: 'Wrong password',
                description: 'Please check the credentials',
                duration: 3,
            };
            notification.open(args);
        };

    }
    componentWillMount(){
        LoginStore.bind(this.loginCheck);
    }
    componentWillUnmount(){
        LoginStore.unbind(this.loginCheck);
    }
    loginCheck(){
        let response = LoginStore.getResponse();
        if(response == 'SUCCESS'){
            hashHistory.push('/users')
        }
        else {
            this.openNotification();
            this.setState({
                buttonText:'Login'
            })
        }
    }
    render(){
        return(
            <div className="login-page login-insport">
                <div className="form">
                    <form onSubmit={(e)=>e.preventDefault()} className="login-form">
                        <input onChange={(e)=>this.setState({id:e.target.value})} value={this.state.id} type="text" placeholder="username"/>
                        <input onChange={(e)=>this.setState({password:e.target.value})}  value={this.state.password}  type="password" placeholder="password"/>
                        <button type="button" onClick={()=>{this.setState({buttonText: 'Loading..'});LoginAction.login({id:this.state.id,password:this.state.password})}}>{this.state.buttonText}</button>
                    </form>
                </div>
            </div>
        )
    }
}